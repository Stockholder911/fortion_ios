//
//  AppDelegate.h
//  FortiON
//
//  Created by Guilherme Vitor on 5/26/15.
//  Copyright (c) 2015 Guilherme Vitor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

